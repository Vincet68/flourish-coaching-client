//
//  HomeworkViewController.swift
//  Flourish Coaching Client
//
//  Created by Vincent Tuckwood on 8/30/19.
//  Copyright © 2019 View Beyond LLC. All rights reserved.
//

import UIKit
import CloudKit

class HomeworkViewController: UIViewController {

  @IBOutlet weak var footerImage: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //Random Footer Image
    footerImage.image = UIImage(imageLiteralResourceName: "grass\(randomIntGenerator())")
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    
  }


}

