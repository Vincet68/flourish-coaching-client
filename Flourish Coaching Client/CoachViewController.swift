//
//  CoachViewController.swift
//  Flourish Coaching Client
//
//  Created by Vincent Tuckwood on 8/30/19.
//  Copyright © 2019 View Beyond LLC. All rights reserved.
//

import UIKit
import CloudKit

class CoachViewController: UIViewController {

  @IBOutlet weak var footerImage: UIImageView!
  @IBOutlet weak var navBar: UINavigationBar!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //Random Footer Image
    footerImage.image = UIImage(imageLiteralResourceName: "grass\(randomIntGenerator())")
    
    navBar.topItem?.title = (coaches[workingCoach][1] as! String)
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    
    
  }


}

