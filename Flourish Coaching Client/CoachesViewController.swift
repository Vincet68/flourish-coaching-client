//
//  CoachesViewController.swift
//  Flourish Coaching Client
//
//  Created by Vincent Tuckwood on 8/30/19.
//  Copyright © 2019 View Beyond LLC. All rights reserved.
//

import UIKit
import CloudKit

var workingCoach = 0

class CoachesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
  @IBOutlet weak var footerImage: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //Random Footer Image
    footerImage.image = UIImage(imageLiteralResourceName: "grass\(randomIntGenerator())")
    
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return coaches.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
    
    cell.textLabel?.text = coaches[indexPath.row][1] as? String
    cell.textLabel?.textColor = .darkGray
    cell.textLabel?.font = UIFont(name: "Arial", size: 17)
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    workingCoach = (coaches[indexPath.row][0] as? Int)!
    
    performSegue(withIdentifier: "CoachesToCoach", sender: self)
    
  }


}

